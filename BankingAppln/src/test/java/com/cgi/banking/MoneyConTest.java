package com.cgi.banking;

import org.junit.Test;
import static org.junit.Assert.*;

public class MoneyConTest {

	private double delta = .1e-15;

	@Test
	public void INRTOUSDTest() {
		MoneyConv MC = new MoneyConv();
		assertEquals(1449.0, Math.round(MC.INRTOUSD(100000.0)), delta);
	}

}
